# Agregar y eliminar elementos HTML

### Agregar elementos

- document.createElement(elemento);
- document.appendChild(elemento);
- document.append(elemento);
- document.replaceChild(nuevo, anterior);

### Eliminar elementos

- elemento.removeChild(elementoChild);
- elemento.remove();