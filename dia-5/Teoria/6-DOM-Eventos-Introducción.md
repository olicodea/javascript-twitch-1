## Eventos - Introducción

El HTML DOM permite que Javascript reaccione a eventos del HTML.

Un ejemplo puede ser, ejecutar código cuando un usuario hace clic en un elemento (evento onclick).

Ejemplos de eventos:

- Cuando carga la página (onload)
- Cuando el mouse se mueve sobre un elemento  (onmouseover)
- Cuando se cambia un input (onchange)
- Cuando se presiona una tecla (keydown)