# DOM

El **DOM** es un estandar para acceder a los documentos.

- El **HTML DOM** es un modelo de objeto estandar donde se definen los elementos HTML como objetos.

- Permite acceder a las propiedades, metodos y eventos de los elementos

- Permite obtener, agregar, cambiar y eliminar elementos HTML