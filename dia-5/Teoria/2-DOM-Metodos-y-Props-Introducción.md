# Métodos y props - Introducción

En el DOM, los elementos HTML se definen como **objects**, que tienen propiedad y métodos a los que se puede acceder con JavaScript.

Una **propiedad** es un valor que se puede obtener o definir.

Un **método** es una acción que se puede realizar.