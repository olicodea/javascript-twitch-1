# Encontrar y modificar elementos HTML

Para acceder a un elemento del HTML primero hay que acceder al objeto document.

### Encontrar elementos

- document.getElementById(id);
- document.getElementsByTagName(tagName);
- document.getElementsByClassName(className);

### Modificar elementos

- element.innerHTML = "Contenido modificado";
- element.attribute = "nuevo valor";
- element.style.property = "nueva regla";
- element.setAttribute(atributo, valor);