## TEMAS

- [x] DOM - Introducción
- [x] DOM - Metodos y propiedades: Introducción
- [x] DOM - Encontrar y modificar elementos
- [x] DOM - Agregar y borrar elementos
- [x] DOM - Buscar elementos con selectores CSS
- [x] DOM - Eventos: Introducción
- [x] DOM - Eventos: EventListener
- [x] Git - Subir repo a gitlab



*Webgrafía:*

https://www.w3schools.com/js/default.asp