# El método addEventListener()

Agrega un detector de eventos a un elemento especificado

## Sintaxis

- elemento.addEventListener(evento, funcion, booleano)
- El 1er parámetro es el tipo de evento ("click" o "load" o cualquier evento del DOM)
- El 2do parámetro es la función que se llamará cuando ocurra el evento
- El 3er parámetro es un booleano que especifica si se usa burbujeo o captura de eventos. Es opcional (Por ahora no lo veremos)
- Ya no se usa el prefijo "on", solo el nombre del evento sin el prefijo. Ejemplo: "load" en lugar de "onload"