const h1 = document.querySelector("#h1");
const buttons = document.querySelectorAll(".btn");

buttons.forEach((btn) => {
    btn.style.background = "red";
    btn.style.width = "300px";
});

function ocultarBtn() {
    const btnAOcultar = document.querySelector("#btnB");
    btnAOcultar.style.display = "none";
}


function modificarTitulo(){
    h1.classList.toggle("d-none");
}

const form = document.getElementById("formulario");
const inputPw = document.createElement("input");
inputPw.type = "password";
inputPw.placeholder = "Escribe un pw";

form.append(inputPw);

inputPw.addEventListener("keydown", function() {
    h1.style.color = "blue"
});

const submitBtn = document.getElementById("submitBtn");

const enviarFormulario = (e) => {
    e.preventDefault();
    console.log(e);
    console.log("Submit...");
}

form.addEventListener("submit", enviarFormulario)