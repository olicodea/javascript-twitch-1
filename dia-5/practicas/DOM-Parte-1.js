const titulo = document.getElementById("h1");
// console.log(titulo.innerHTML);

// titulo.innerHTML = "TITULO MODIFICADO"
// titulo.innerText = "MODIFICANDO TEXTO"
// titulo.textContent = "TEXT CONTENT";

const inputs = document.getElementsByTagName("input");
// console.log(inputs)

const btns = document.getElementsByClassName("btn");
// console.log(btns);

const inputNombre = document.getElementById("nombre");

inputNombre.setAttribute("disabled", true);

inputNombre.removeAttribute("disabled");

inputNombre.id = "nombreNuevo";

// inputNombre.style.backgroundColor = "red"
// inputNombre.style.display = "none"

const form = document.getElementById("formulario");

const inputPw = document.createElement("input");
inputPw.type = "password";
inputPw.placeholder = "Escribe una contraseña";

form.append(inputPw);

const botonA = document.getElementById("botonA");

botonA.remove();

form.removeChild(inputPw);

