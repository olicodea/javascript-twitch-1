/**
 * TIPOS DE DATO - Datatypes
 *
 * 1 - Number
 * 2 - String
 * 3 - Boolean
 * 4 - Undefined
 * 5 - Null
 * 6 - Object
 *
 * 7 - Symbol
 * 8 - Bigint
 *
 * Objects:
 *  1 - Object
 *  2 - Array
 *  3 - Date
 */

//Number
let n1 = 2;

//String
let nombre = "Juan";

//Boolean
const verdadero = true;
const falso = false;

// console.log(typeof falso)

// Null
let NoTieneNada = null;
// console.log(typeof NoTieneNada)

let objecto = {
    numero1: "",
    numero: {},
    nombre: [],
    apellido: "Gt",
};

// console.log(objecto)
// console.log(objecto.nombre)

let array = ["elemento 1", "elemento 2", "elemento 3", "manzana"];
let celulares = ["Iphone X", "Iphone 12", "Iphone 14", "Moto G100"];
let numeros = [
    3,
    5,
    100,
    3550,
    "Elemento x",
    "Manzana",
    "Aceitunas",
    true,
    false,
    nombre,
];

// console.log(numeros);
// console.log(numeros[2])
// console.log(celulares[0])

// Date

const diaActual = new Date();
const fechaCustom = new Date('2023-12-31');

console.log(fechaCustom)