//Condicionales

//IF ELSE
//
let valor = true;
let numero = 20;
let numero2 = 40;

// == o ===
function compararSiDosNumerosSonIguales(num1, num2) {
    if (num1 === num2) {
        // console.log(valor)
        console.log("La condición dio resultado VERDADERO");
    } else {
        // console.log(valor)
        console.log("La condición dio resultado FALSO");
    }
}

// compararSiDosNumerosSonIguales(2, 4)
// compararSiDosNumerosSonIguales(2, 2)

function compararSiUnNumeroEsMayorQueOtro(num1, num2) {
    if (num1 > num2) {
        console.log(num1 + " Es mayor que: " + num2);
    } else {
        console.log("No es mayor");
    }
}

// compararSiUnNumeroEsMayorQueOtro(2,5)
// compararSiUnNumeroEsMayorQueOtro(5, 2)

function esMenor(num1, num2) {
    if (num1 <= num2) {
        console.log("Es menor o igual");
    } else {
        console.log("No es menor o igual");
    }
}

// esMenor(3,1)

// === igual
// <== menor o igual que
// >== mayor o igual que
// < menor que
// > mayor que
// != distinto de 

if(valor1 != valor2){
    console.log("Distintos")
}