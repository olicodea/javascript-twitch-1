// Bucles e Iteracion - For

const verdurasFrutas = [
    "Manzana", //0
    "Pera", //1
    "Lechuga", //2
    "Morrón", //3
    "Tomate",
    "Zapallo",
    "Banana",
    "Pizza",
    "Kiwi",
    "Coco",
];

// console.log(verdurasFrutas.length)

function iteararVerdurasConFor() {
    for (let i = 0; i < verdurasFrutas.length; i++) {
        console.log(`i: ${i} - verdura: ${verdurasFrutas[i]}`);
    }
}

function iterarConForEach() {
    verdurasFrutas.forEach((verduraFruta, idx) => console.log(idx, verduraFruta));
}

const dato = prompt("Ingrese un dato")

// iterarConForEach()