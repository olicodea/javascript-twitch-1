// IF ELSE

const numero1 = 4;
const numero2 = 6;

// iguales ->  === o ==

function comparaQueDosNumerosSeanIguales(num1, num2) {
    if (num1 == num2) {
        console.log("SON IGUALES");
    }
}

function comparaQueSeanEstrictamenteIguales(num1, num2) {
    if (num1 === num2) {
        console.log("SON IGUALES");
    } else {
        console.log("NO SON IGUALES");
    }
}

const comparaQueSeanEstrictamenteDistintos = (num1, num2) => {
    // Distinto de -> !==
    if (numero1 !== numero2) {
        console.log("SON DISTINTOS");
    } else {
        console.log("NO SON DISTINTOS");
    }
};

// mayor o igual: >=

const mayorOIgual = (num1, num2) => {
    if (numero1 >= numero2) {
        console.log(`${numero1} es mayor o igual que ${numero2}`);
    }
}

// if (20 > numero2) {
//     console.log(`20 es mayor que ${numero2}`);
// }

//menor o igual : <=

const menorOIgual = (num1, num2) => {
    if (numero1 <= numero2) {
        console.log(`${numero1} es menor o igual que ${numero2}`);
    }
}

/// Diferencia entre funcion regulgar y funcion flecha

function miFuncion(parametro1){
    return "Mi función " + parametro1;
}

const miFuncion2 = param1 => `Mi función ${param1}`;
// console.log(miFuncion2(2))

