// DOM - Document Object Model

const h1 = document.getElementById("h1")
h1.innerText = "JAVASCRIPT DESDE CERO - YAPA"

const nav = document.querySelector(".nav")
nav.style.display = "none"

const btn = document.querySelector("#probando");
btn.addEventListener("click", (e) => {
    alert("PROBANDO EL DOM - Evento onclick")
})