//Condicionales - SWITCH

// If anidados

const verdurasFrutas = [
    "Manzana",
    "Pera",
    "Lechuga",
    "Morrón",
    "Tomate",
    "Zapallo",
    "Banana",
    "Pizza",
    "Kiwi",
    "Coco",
];

// if(verdurasFrutas.length > 0){
//     if(verdurasFrutas[0] === "Manzana"){
//         console.log(verdurasFrutas[0])
//     } else {
//         console.log("NO ES UNA MANZANA")
//     }
// } else if(verdurasFrutas.length === 100){
//     console.log("No es igual a 100")
// }

const compararVerdurasYFrutas = (verdura) => {
    if (verdura == verdurasFrutas[0]) {
        console.log(`${verdura} es igual a ${verdurasFrutas[0]}`);
    } else if (verdura == verdurasFrutas[1]) {
        console.log(`${verdura} es igual a ${verdurasFrutas[1]}`);
    } else if (verdura == verdurasFrutas[2]) {
        console.log(`${verdura} es igual a ${verdurasFrutas[2]}`);
    } else if (verdura == verdurasFrutas[3]) {
        console.log(`${verdura} es igual a ${verdurasFrutas[3]}`);
    } else if (verdura == verdurasFrutas[4]) {
        console.log(`${verdura} es igual a ${verdurasFrutas[4]}`);
    } else if (verdura == verdurasFrutas[5]) {
        console.log(`${verdura} es igual a ${verdurasFrutas[5]}`);
    } else {
        console.log(`${verdura} es igual a ${verdurasFrutas[6]}`);
    }
};

function compararVerduras(verdura) {
    switch (verdura) {
        case "Manzana":
            console.log("ES UNA MANZANA");
            console.log("ES UNA MANZANA")
            break;

        case "Pera":
            console.log("ES UNA PERA");
            break;

        case "Lechuga":
            console.log("ES UNA LECHUGA");
            break;
        default:
            console.log("NO SE ENCONTRÓ LA VERDURA")
            break;
    }
}

// compararVerduras("Pizza")
// compararVerduras("Manzana")

