// Metodos de array

// forEach

const utilesEscolares = [
    "Lápiz", "Goma", "Regla", "Liquid Paper", "Lapicera", "Fibras", "Fibrones"];

function recorrerConForEach(array){
    array.forEach(element => {
        console.log(element)
    });
}

// recorrerConForEach(utilesEscolares);

const utilesConMap = utilesEscolares.map((util) => {
    const utilNuevo = {
        nombre: util,
        marca: "Faber"
    }

    return utilNuevo;
});

// console.log(utilesConMap);

// Metodo filter

const utilesFiltrados = utilesEscolares.filter(util => util.startsWith("L"));

// console.log(utilesFiltrados);

// Reduce

const numeros = [1, 2, 2, 4]

const sum = numeros.reduce((acumulado, numActual) => acumulado + numActual, 0)

// console.log(sum);


// console.log(utilesEscolares.sort());
// console.log(utilesEscolares.reverse())

const newArray = [...utilesEscolares].sort();

console.log(utilesEscolares);