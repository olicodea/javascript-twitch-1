const fecha = {
    anio: "",
    mes: "",
    dia: "",
};
const fecha_nac = document.querySelector("#fecha_nac");
const form = document.querySelector(".form");
const main = document.querySelector(".container");

fecha_nac.addEventListener("change", setFecha);

function setFecha(e) {
    e.preventDefault();
    const date = e.target.valueAsDate;
    const [month, day, year] = [
        date.getMonth() + 1,
        date.getDate(),
        date.getFullYear(),
    ];
    
    fecha.anio = year;
    fecha.mes = month;
    fecha.dia = day;
}

function mostrarFechaPorSeparado(e) {
    e.preventDefault();
    const sectionDos = document.createElement("section");
    sectionDos.classList.add("section");

    const parrafoFecha = document.createElement("p");
    parrafoFecha.innerHTML = `El año es ${fecha.anio}. El mes es: ${fecha.mes}. El día es: ${fecha.dia}`;

    sectionDos.append(parrafoFecha);
    main.append(sectionDos);
}

form.addEventListener("submit", mostrarFechaPorSeparado);
